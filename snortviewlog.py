#!/usr/bin/python3
"""
Snort Log Viewer - with filters and descriptions
"""

import os
import sys
import time
import json
import pandas as pd
import socket  # to discover local network
from subprocess import check_output  # to use output of system commands
from PyQt5.QtCore import QAbstractTableModel, Qt
#from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMainWindow, QApplication, QTableView
import SnortView




userName = "ben"
dataPath = os.path.join("/home", userName, "snort")
logPath = "/var/log/snort"
logFile = "alert_json.txt"
sid_map = "/usr/local/etc/snort/sid-msg.map"


allColumns = ['timestamp src_addr src_port dst_addr dst_port service rule priority class gid sid rev target action ']  # set these in alert_json section of snort.lua config

selectedColumns = ['timestamp', 'src_addr', 'dst_addr', 'rule', 'action']


class guiApp(QMainWindow, SnortView.Ui_SnortView):
    def __init__(self, parent=None):
        
        super(guiApp, self).__init__(parent)
        self.setupUi(self)
        self.get_my_ip()
        print("Loading %s" % logFile)
        self.load_logfile()
        #print(self.df)
        self.get_log_filters()
        self.set_visible_columns()
        print("Source: ", self.destinationIPfilter.currentText(), "Dest: ")
        
        
        self.model = pandasModel(self.df[self.selectedColumns])
        self.logTableView.setModel(self.model)
        self.logTableView.setColumnWidth(0, 200)
        
        self.sourceIPfilter.activated.connect(self.apply_filter)
        self.destinationIPfilter.activated.connect(self.apply_filter)
        self.ruleFilter.activated.connect(self.apply_filter)
        # log selection triggers
        self.logTableView.doubleClicked.connect(self.show_table_info)
        
        
    def apply_filter(self):
        sourceSelected = self.sourceIPfilter.currentText()
        destSelected = self.destinationIPfilter.currentText()
        ruleSelected = self.ruleFilter.currentText()
        if ruleSelected != 'no filter':
            dfTemp = self.df.loc[self.df['rule'] == ruleSelected]
        else:
            dfTemp = self.df
        # now apply the sourceIPfilter
        if sourceSelected != 'no filter':
            dfTemp = self.df.loc[self.df['src_addr'] == sourceSelected]
        else:
            pass
        # now apply the destinationIPfilter
        if destSelected != 'no filter':
            dfTemp = dfTemp.loc[self.df['dst_addr'] == destSelected]
        else:
            pass
        
        print(dfTemp)
        self.model = pandasModel(dfTemp[self.selectedColumns])
        self.logTableView.setModel(self.model)
        self.logTableView.setColumnWidth(0, 200)
        



    def get_log_filters(self):
        """
        generates lists of unique IP addresses and rule alerts from the logFile, populates sourceList, destList, ruleList & actionList
        and puts them in the GUI combo boxes
        sourceIPfilter, destIPfilter, ruleFilter, actionFilter
        """
        #dftemp = self.df.astype(str)

        actionList = self.df.astype(str)['action'].unique().tolist()
        actionList.sort()
        if 'nan' in actionList:
            actionList.remove('nan')
        actionList.insert(0,"no filter")

        sourceList = self.df.astype(str)['src_addr'].unique().tolist()
        sourceList.sort()
        if 'nan' in sourceList:
            sourceList.remove('nan')
        sourceList.insert(0,"no filter")

        destList = self.df.astype(str)['dst_addr'].unique().tolist()
        destList.sort()
        if 'nan' in destList:
            destList.remove('nan')
        destList.insert(0, "no filter")

        ruleList = self.df.astype(str)['rule'].unique().tolist()
        ruleList.sort()
        if 'nan' in ruleList:
            ruleList.remove('nan')
        ruleList.insert(0,"no filter")
        
        self.actionFilter.addItems(actionList)
        self.ruleFilter.addItems(ruleList)
        self.sourceIPfilter.addItems(sourceList)
        self.destinationIPfilter.addItems(destList)


    def set_visible_columns(self):
        """
        defines the columns to be used when generating the model for logTableView
        """
        self.selectedColumns = selectedColumns


    def get_my_ip(self):
        """
        returns list of IP addresses from 'hostname -I' system command
        and adds items to myIPcomboBox
        """
        myIPlist = check_output(['hostname', '-I']).decode("utf-8").split()
        print("my IP is: ", myIPlist)
        self.myIPcomboBox.addItems(myIPlist)

    def load_logfile(self):
        with open(os.path.join(logPath,logFile), "r") as log:
            self.df =pd.read_json(log, lines=True)

    def show_table_info(self):
        cell = self.logTableView.selectedIndexes()[0]
        print("Row: ", cell.row())
        print("Col:", cell.column())
        columnsVisible = self.logTableView.model().columnCount(None)
        #print(columnsVisible)
        #select the entire row
        #for i in range(columnsVisible):
        self.logTableView.selectRow(cell.row())
        #value = self.logTableView.model().data(cell)
        #print(value)
        #read the row
        colTitle = selectedColumns[cell.column()]
        print(colTitle)
        
        
        if colTitle == 'sid':
            #print(colIndex)
            sid = cell.data()
            print("SID: ", sid)
            self.lookup_sid_map(sid)
        elif colTitle == 'rule':
            sid = cell.data().split(':')[1]
            print("SID: ", sid)
            self.lookup_sid_map(sid)
        elif colTitle == 'dst_addr':
            if self.is_public_ip(cell.data()):
                self.geolocate_IP(cell.data())
        elif colTitle == 'timestamp':
            # print the log line
            self.lookup_log_line(cell.data())

    def lookup_log_line(self, timestamp):
        self.infoListWidget.addItem("Looking up timestamp: %s" % timestamp)
        with open(os.path.join(logPath,logFile), "r") as log:
            for line in log:
                print(timestamp)
                #line = line.replace("\n", "")
                if timestamp in line:
                    print(line)
                    self.infoListWidget.addItem(line)


    def lookup_sid_map(self, sid):
        self.infoListWidget.addItem("Looking up rule: %s" % sid)
        with open(sid_map, "r") as map:
            for line in map:
                line = line.replace("\n", "")
                if line[0] != "#":
                    parts = line.split('||')
                    # print(parts)
                    # print(parts[0])
                    rowIndex = parts.pop(0).strip()
                    # print(rowIndex)
                    if rowIndex == sid:
                        newRow = {'sid':rowIndex}
                        for part in parts:
                            part = part.strip()
                            if "," in part:
                                key,value = part.split(",", 1)
                                newRow[key] = value
                            else:
                                newRow['message'] = part

                        self.infoListWidget.addItems(parts)
                        #print(newRow)
        
        
        
    def is_public_ip(self, IP):
        """
        TODO: check if the IP public
        """
        return True

    def geolocate_IP(self, IP):
        output = "Looking up IP address: " + IP
        self.infoListWidget.addItem(output)





class pandasModel(QAbstractTableModel):
    """
    https://learndataanalysis.org/display-pandas-dataframe-with-pyqt5-qtableview-widget/
    
    """

    def __init__(self, data):
        QAbstractTableModel.__init__(self)
        self._data = data

    def rowCount(self, parent=None):
        return self._data.shape[0]

    def columnCount(self, parnet=None):
        return self._data.shape[1]

    def data(self, index, role=Qt.DisplayRole):
        if index.isValid():
            if role == Qt.DisplayRole:
                return str(self._data.iloc[index.row(), index.column()])
        return None

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self._data.columns[col]
        return None
    
    


def main():
    check_folders(dataPath)
    
    
    #myHostname = socket.gethostname()
    #print(myHostname)

    #sourceList, destList = get_peers_list(df)
    
    #ruleList = get_alerted_rules(df)
    #for rule in ruleList:
        #print(rule)
    #for ip in destList:
        #print(ip)
    
    #print(df[df.isin([myIP]).any(axis=1)])
    #print(df[selectedColumns])
    
    #sidList = []
    #for rule in ruleList:
        #sidList.append(rule.split(":")[1])
    
    #read_sid_map(sidList)
    
    
    
    app = QApplication(sys.argv)
    # put the df in a model that qt can use
    window = guiApp()
    
    
    

    
    window.show()
    app.exec_()
    
    
    
    


def read_sid_map(sidList):
    with open(sid_map, "r") as map:
        dfSidMap = pd.DataFrame(columns=['sid', 'message'])
        #dfSidMap.set_index('sid', inplace=True)
        for line in map:
            line = line.replace("\n", "")
            if line[0] != "#":
                parts = line.split('||')
                #print(parts)
                #print(parts[0])
                rowIndex = parts.pop(0).strip()
                
                #print(rowIndex)
                if rowIndex in sidList:
                    newRow = {'sid':rowIndex}
                    for part in parts:
                        part = part.strip()
                        if "," in part:
                            key,value = part.split(",", 1)
                            newRow[key] = value
                                
                        else:
                            newRow['message'] = part

                    print(newRow)
                    dfSidMap = dfSidMap.append(newRow, ignore_index=True)
    #print(dfSidMap)
    input('Finished')


def get_alerted_ids(df):
    """
    returns a list of unique sid in the logFile
    """
    ruleList = df['rule'].unique()
    
    return ruleList


def get_alerted_rules(df):
    """
    returns a list of unique rules in the logFile
    """
    ruleList = df['rule'].unique()
    return ruleList









def check_folders(dataPath):
    if os.path.isdir(dataPath):
        print("Data folder exists")
    else:
        os.makedirs(dataPath)


if __name__ == "__main__":
    main()
else:
    print("this file should be run directly")

