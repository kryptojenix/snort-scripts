#!/bin/bash

echo '-------------------- log ----------------------'
for file in ./snort.log.*; do
  echo $file 
  u2spewfoo $file
done

echo '------------------- alert ---------------------'


for file in ./snort.alert.*; do
  echo ''
  echo $file
  u2spewfoo $file |grep sig | sed -e 's/.*sig id:\(.*\)gen.*/\1/'| tr -d [:blank:] | sort | uniq

  echo '' 
  u2spewfoo $file |grep sig | sed -e 's/.*sig id:\(.*\)gen.*/\1/'| tr -d [:blank:] | sort | uniq | while read -r result
  do
    # echo alert sid = $result
    # echo 'sid:'$result';' 
    grep 'sid:'$result';' /etc/snort/rules/snort.rules | sed -e 's/.*msg:\(.*\)icode.*/\1/'
  done

done

echo '------------------- dump ----------------------'

for file in ./tcpdump.log.*; do
  echo $file
#  snort -r $file
done



# search the rules to display which one triggered
# grep -e 'sid:408;' -rn /etc/snort/rules/
