#!/bin/bash

#TODO: check if run as root
echo "You don't need to run this as root. Please run as a normal user with sudo privileges"

echo "check system variables"
echo "$(dirname "$0")"
# get username
# https://stackoverflow.com/questions/20504662/how-to-get-home-directory-of-different-user-in-bash-script

#username="rvadmin"
#echo $username


echo "Running this script as: "
# Current user should be rvadmin
# echo "Current user: `whoami`"
echo "Current user: $USER"
# currentUser = $(whoami)
# echo "Current user: `id`"
echo 

# get home folder
# 
# echo "Current user: $currentUser" 
homeFolder="/home/$USER"
# echo "Home folder is $homeFolder"


ruleFolder="$homeFolder/snort/rules"
dataFolder="$homeFolder/snort/data"
logFolder="/var/log/snort"
echo "Saving data to: $dataFolder"
echo "Home folder is $homeFolder"


# logList=$(sudo find $logFolder -name '*.log.*') 
# echo $logList
# alertList=$(sudo find $logFolder -name '*.alert.*')
# echo $alertList
bigList=$(sudo find $logFolder -name '*')
echo $bigList

for f in $bigList; do
    # shorten filename for outputFile
    f2=$(basename $f)
    echo "File -> $f2"
    # do the copy
    sudo cp $f $dataFolder
    if [[ $f2 == *"tcpdump"* ]]; then
        echo "skipping spew creation for $f2"
    else
        outputFile="$dataFolder/$f2.spew"
        echo "creating spew file: $outputFile"
        u2spewfoo $file > $outputFile
    fi
    # ---- if the file is a tcpdump ---
done

# update local copy of rules
sudo rsync -ru --delete /usr/local/etc/rules* ~/snort/rules/

echo "Setting permissions"
#TODO: get current user for ownership
sudo chown -R snort:"$USER" $ruleFolder/*
sudo chown -R snort:"$USER" $dataFolder/*
sudo chmod 664 $dataFolder/*
