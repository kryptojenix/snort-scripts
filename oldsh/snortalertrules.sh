#!/bin/bash

echo '------------------- snort view alerts ---------------------'


for file in /var/log/snort/snort.alert.*; do
  echo ---------------- $file -----------------------
  u2spewfoo $file |grep source
  u2spewfoo $file |grep sig | sed -e 's/.*sig id:\(.*\)gen.*/\1/'| tr -d [:blank:] | sort | uniq | while read -r result
  do
    # echo alert sid = $result
    # echo 'sid:'$result';' 
    grep 'sid:'$result';' /etc/snort/rules/* | sed -e 's/.*msg:\(.*\)icode.*/\1/'
  done
done



