#!/bin/bash
# views the files produced by snortcopylog.sh in the ~/snort/data/ 
# directory. may become a useful tool, but not integral to the python
# code


echo ''
echo '-------------------- log files ----------------------'
counter=0
echo "# new file header" > ~/snort/data/snortloglist.txt
for file in ~/snort/data/snort.log.*; do
    if [[ $file == *"spew"* ]]; then
        echo "skipping $file"
    else
        let counter++
        echo $file >> ~/snort/data/snortloglist.txt
        #u2spewfoo $file
        
        echo ''
        echo $file
        eventCounter=0
        while read -r result; do
            #echo $result
            let eventCounter++
        done < <(u2spewfoo $file) 
        echo "counted $eventCounter events"
        #|grep sig | sed -e 's/.*sig id:\(.*\)gen.*/\1/'| tr -d [:blank:])
        
    fi
        
        
        
done
echo "$counter log files"
echo ''
echo '------------------- alert files ---------------------'

counter=0
echo "# new file header" > ~/snort/data/alertloglist.txt

for file in ~/snort/data/snort.alert.*; do

    if [[ $file == *"spew"* ]]; then
        echo "skipping $file"
    
    else
        echo $file >> ~/snort/data/alertloglist.txt
        #u2spewfoo $file
        echo ''
        while read -r result; do
            #echo $result
            let counter++
        done < <(u2spewfoo $file |grep sig | sed -e 's/.*sig id:\(.*\)gen.*/\1/'| tr -d [:blank:])
    
    #  u2spewfoo $file |grep source
        echo 'found '$counter' total alerts'
        echo ''
        let counter=0
        while read -r result; do
            let counter++
            # echo alert sid = $result
            #echo 'sid:'$result';' 
            grep 'sid:'$result';' /etc/snort/rules/* | sed -e 's/.*msg:\(.*\)icode.*/\1/'
        done < <(u2spewfoo $file |grep sig | sed -e 's/.*sig id:\(.*\)gen.*/\1/'| tr -d [:blank:] | sort | uniq)
        echo 'found ' $counter ' unique alerts'
        echo ''
    fi
    
done
echo '------------------- TCP dumps ----------------------'
let counter=0
for file in ~/snort/data/tcpdump.log.*; do
    let counter++
    echo $file >> ~/snort/data/tcpdumplist.txt
done
echo "$counter total tcp dumps"    
echo ''
echo ''
