#!/bin/bash
echo "checking for previous snort-scripts config"
installed=$(pwd)

sudo apt install -y rsync

if [ ! -d ~/snort ]; then
    echo "Creating new install folder /home/$USER/snort"
    mkdir ~/snort
else
    echo "previous install detected"
fi

    echo "waiting for a second"
    sleep 1

echo "updating config files"
if [ ! -d ./config ]; then
    mkdir ./config
fi


    if [ -f ./config/snort.conf ]; then
        echo "keeping current MODIFIED snort config"
    else
        echo "re-loading snort config from the installer"
        cp ./examples/snort.conf ./config/
    fi
    
    if [ ! -d /etc/snort ]; then
        sudo mkdir /etc/snort
    fi
    if [ ! -d /etc/snort/rules ]; then
        sudo mkdir /etc/snort/rules
    fi
    
    if [ ! -d /etc/snort/so_rules ]; then
        sudo mkdir /etc/snort/so_rules
    fi
    
    if [ ! -d /etc/snort/preproc_rules ]; then
        sudo mkdir /etc/snort/preproc_rules
    fi
    
    if [ -f /etc/snort/snort.conf ]; then
        echo "you have an existing snort config file at /etc/snort/"
        echo "it will be backed up as /etc/snort/snort.conf.old"
        echo "and replaced with the version from the installer"
        sudo mv /etc/snort/snort.conf /etc/snort/snort.conf.old
    fi
    sudo cp ./config/snort.conf /etc/snort/
    
    if [ ! -f /etc/snort/classification.config ]; then
        if [ ! -f ./config/classification.config ]; then
            cp ./examples/classification.config ./config
        fi
        echo "copying classification.config"
        sudo cp ./config/classification.config /etc/snort/
    else
        echo "keeping existing classification.config"
    fi
    echo
    
    if [ ! -f /etc/snort/rules/local.rules ]; then
        if [ ! -f ./config/local.rules ]; then
            cp ./examples/local.rules ./config
        fi
        echo "copying local.rules"
        sudo cp ./config/local.rules /etc/snort/rules/
    else
        echo "keeping existing local.rules"
    fi
    echo
    
    if [ ! -f /etc/snort/rules/deleted.rules ]; then
        if [ ! -f ./config/deleted.rules ]; then
        cp ./examples/deleted.rules ./config
        fi
        echo "copying deleted.rules"
        sudo cp ./config/deleted.rules /etc/snort/rules/
    else
    echo "keeping existing deleted.rules"
        fi
    echo
    
    if [ ! -f /etc/snort/reference.config ]; then
        if [ ! -f ./config/reference.config ]; then
        cp ./examples/reference.config ./config
        fi
        echo "copying reference.config"
        sudo cp ./config/reference.config /etc/snort/
        else
        echo "keeping existing reference.config"
    fi
    echo
    
    if [ -f ./config/snort.debian.conf ]; then
        echo "keeping current MODIFIED debian config"
    else
        echo "re-loading debian config from the installer"
        cp ./examples/snort.debian.conf ./config/
    fi
    echo 
    # input snippit from https://alvinalexander.com/linux-unix/shell-script-how-prompt-read-user-input-bash
    while true
        do
            echo "edit your network config in snort-scripts/config/snort.debian.conf BEFORE continuing....."
            read -p "Finished updating config? " answer
            # echo "$installed/config/pulledpork.conf"
            #  copy the config file now
            
            case $answer in
            [yY]* ) sudo cp $installed/config/snort.debian.conf /etc/snort/
            break;;

        [nN]* ) exit;;
        
        * )     echo "N to skip updating file";;
        esac
    done
    if [ -f /etc/snort/snort.debian.conf ]; then
        echo "you have an existing debian config file at /etc/snort/"
        echo "it will be backed up as /etc/snort/snort.debian.conf.old"
        echo "and replaced with the version from the installer"
        sudo mv /etc/snort/snort.debian.conf /etc/snort/snort.debian.conf.old
    fi
    sudo cp ./config/snort.debian.conf /etc/snort
    
    
    if [ -f ./config/pulledpork.conf ]; then
        echo "keep current MODIFIED pulledpork config"
    else
        echo "re-loading pulledpork config from the repo"
        cp ./examples/pulledpork.conf ./config/pulledpork.conf
    fi
    
    if [ -f /etc/snort/pulledpork.conf ]; then
        echo "you have an existing pulledpork config file at /etc/snort/"
        echo "it will be backed up as /etc/snort/pulledpork.conf.old"
        echo "and replaced with the version from the installer"
        sudo mv /etc/snort/pulledpork.conf /etc/snort/pulledpork.conf.old
    fi

# check the pulledpork installation
if [ -f /usr/local/bin/pulledpork.pl ]; then
    echo "PulledPork found"
    if [ ! -d ~/snort/pulledpork ]; then
        echo "PulledPork installation folder not found in the expected location, a new clone of the git repo will be created at ~/snort/pulledpork/"
        cd ~/snort/
        git clone https://github.com/shirkdog/pulledpork
        cd pulledpork
    else
        cd ~/snort/pulledpork
        git pull
    fi
else
    echo "PulledPork not found, installing now"

    if [ ! -d ../pulledpork ]; then
        cd ~/snort/
        git clone https://github.com/shirkdog/pulledpork
        cd pulledpork
    else
        cd ~/snort/pulledpork
        git pull
    fi
    echo "Installing the PulledPork script"
    sudo cp ./pulledpork.pl /usr/local/bin
fi

sudo chmod +x /usr/local/bin/pulledpork.pl
sudo cp ~/snort/pulledpork/etc/*.conf /etc/snort/

# the old debian version has heaps of rules files in /etc/snort/rules
# we only want to see 3 rules files
echo "Cleaning up rules directory"
find /etc/snort/rules -type f ! -name 'snort.rules' ! -name 'deleted.rules' ! -name 'local.rules'  -exec sudo rm -f {} +

# input snippit from https://alvinalexander.com/linux-unix/shell-script-how-prompt-read-user-input-bash
while true
do
    echo
    echo
    echo
    # (1) prompt user, and read command line argument
    #TODO: input the oinkcode and edit that into the config file
    echo "Add your oinkcode to pulledpork.conf now at (cloned folder)/snort-scripts/config/pulledpork.conf BEFORE continuing....."
    read -p "Run PulledPork now? " answer
    # echo "$installed/config/pulledpork.conf"
    #  copy the config file now
    sudo cp $installed/config/pulledpork.conf /etc/snort/

    # (2) handle the input we were given
    case $answer in
    [yY]* ) sudo pulledpork.pl -P -c /etc/snort/pulledpork.conf
        # run pulledpork
        break;;

   [nN]* ) exit;;

   * )     echo "Enter Y or N, please.";;
  esac
done

# restart snort service
sudo systemctl restart snort3.service
systemctl status snort3.service

# manage cron jobs to update snort rules
#sudo cp /etc/crontab /etc/crontab.old
#sudo echo "44 15 * * * pulledpork.pl -c pulledpork.conf -i disablesid.conf -T -H" >> /etc/crontab


