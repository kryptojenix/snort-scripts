# How to install snort3 and pulledpork on Ubuntu 20.04


### snort version in the repos is old, need to compile from source!
https://kifarunix.com/install-and-configure-snort-3-nids-on-ubuntu-20-04/
some more config help here
https://www.securitynik.com/2021/02/snort3-on-ubuntu-20-housekeeping-appid.html

go to the snort.ogr website (sign up if you don't have an account)
`https://snort.org/oinkcodes`
chose the latest 3.1.x.x version and go to https://snort.org/downloads and download the matching source
`sudo apt install build-essential libpcap-dev libpcre3-dev libnet1-dev zlib1g-dev luajit hwloc libdnet-dev libdumbnet-dev bison flex liblzma-dev openssl libssl-dev pkg-config libhwloc-dev cmake cpputest libsqlite3-dev uuid-dev libcmocka-dev libnetfilter-queue-dev libmnl-dev autotools-dev libluajit-5.1-dev libunwind-dev`
## install snort 3.1.0.0
#### make a folder for the source files
`mkdir ~/src && cd ~/src`

#### compile and install the libdaq dependency
`git clone https://github.com/snort3/libdaq.git`
`cd libdaq`
`./bootstrap`
`./configure`
`make`
`sudo make install`
`cd ..`

#### compile and install snort3
`mkdir snort3 && cd snort3`
`wget https://snort.org/downloads/snortplus/snort3-3.1.0.0.tar.gz`
`tar zxf snort3-3.1.0.0.tar.gz`
`cd snort3-3.1.0.0`
`./configure_cmake.sh --prefix=/usr/local`
`cd build`
`make`
`sudo make install`
`sudo ldconfig`

check which version you have
`snort -V`

#### set the interface in promiscuous mode

`sudo ip link set wlp4s0 promisc on`
test with
`ethtool -k wlp4s0 | grep receive-offload`
`sudo ethtool -K wlp4s0 gro off lro off`
check again with
`ethtool -k wlp4s0 | grep receive-offload`
make persistant

`sudo nano /etc/systemd/system/snort3-nic.service`
paste the following
```
[Unit]
Description=Set Snort 3 NIC in promiscuous mode and Disable GRO, LRO on boot
After=network.target

[Service]
Type=oneshot
ExecStart=/usr/sbin/ip link set dev wlp4s0 promisc on
ExecStart=/usr/sbin/ethtool -K wlp4s0 gro off lro off
TimeoutStartSec=0
RemainAfterExit=yes

[Install]
WantedBy=default.target
```
enable the new config

`sudo systemctl daemon-reload`
`sudo systemctl enable --now snort3-nic.service`

disable it with
`sudo systemctl disable snort3-nic.service`


#### download the community rules
`cd ~/src`
`mkdir rules && cd rules`
`wget https://www.snort.org/downloads/community/snort3-community-rules.tar.gz`
`sudo mkdir /usr/local/etc/rules`
`sudo tar xzf snort3-community-rules.tar.gz -C /usr/local/etc/rules/`

#### setup the rules
`sudo nano /usr/local/etc/snort/snort.lua`
set HOME_NET to `192.168.20.0/24`
set EXTERNAL_NET to `!$HOME_NET`

set the rules the `ips =` section
`include = '/usr/local/etc/rules/snort3-community-rules/snort3-community.rules'`

#### edit snort default config
`sudo nano /usr/local/etc/snort/snort_defaults.lua`
TODO: don't have any info for what to change yet.



#### install snort OpenAppID - optional
`cd ~/src`
`wget https://snort.org/downloads/openappid/16584 -O OpenAppId-16584.tgz`
`tar -xzvf OpenAppId-16584.tgz`
`sudo cp -R odp /usr/local/lib/`

edit the snort.lua config
`sudo nano /usr/local/etc/snort/snort.lua`
and add the following config
```
appid =
{
    -- appid requires this to use appids in rules
    --app_detector_dir = 'directory to load appid detectors from'
    app_detector_dir = '/usr/local/lib',
    log_stats = true,
    
    }
```


#### validate config
`snort -c /usr/local/etc/snort/snort.lua`
you should see
```
Loading /usr/local/etc/rules/snort3-community-rules/snort3-community.rules:
Finished /usr/local/etc/rules/snort3-community-rules/snort3-community.rules:
```
and some `rule counts`

#### Create a custom rule for testing
`sudo nano /usr/local/etc/rules/local.rules`
and paste the following to detect pings
`alert icmp any any -> $HOME_NET any (msg:"ICMP connection test"; sid:1000001; rev:1;)`

check the syntax
`snort -c /usr/local/etc/snort/snort.lua -R /usr/local/etc/rules/local.rules`
and run snort with only this rule
`sudo snort -c /usr/local/etc/snort/snort.lua -R /usr/local/etc/rules/local.rules -i wlp4s0 -A alert_fast -s 65535 -k none`
open a new terminal and ping the snort machine

#### set up logging
`sudo mkdir /var/log/snort`
edit snort config
`sudo nano /usr/local/etc/snort/snort.lua`
and go to section --7. configure outputs
enable some logging
```
-- alert_fast = { file = true, packet = false, limit = 10,}
alert_json =
{
    fields = 'timestamp src_addr src_port dst_addr dst_port service rule priority class gid sid rev target action ',
    file = true,
    limit = 100,
    }
```

all of the available json logging fields are:
    fields = 'seconds timestamp action class b64_data dir dst_addr dst_ap dst_port eth_dst eth_len eth_src eth_type gid icmp_code icmp_id icmp_seq icmp_type iface ip_id ip_len msg mpls pkt_gen pkt_len pkt_num priority proto rev rule service sid src_addr src_ap src_port target tcp_ack tcp_flags tcp_len tcp_seq tcp_win tos ttl udp_len vlan'




save and check syntax
`snort -c /usr/local/etc/snort/snort.lua`


### run snort as a service
add a nologin system user for snort
`sudo useradd -r -s /usr/sbin/nologin -M -c SNORT_IDS snort`
set ownership of the logs
`sudo chmod -R 5775 /var/log/snort`
`sudo chown -R snort:snort /var/log/snort`
test that logs are working with
`sudo snort -c /usr/local/etc/snort/snort.lua -R /usr/local/etc/rules/local.rules -i wlp4s0 -s 65535 -k none -l /var/log/snort/`
create a systemd service
`sudo nano /etc/systemd/system/snort3.service`
and paste the following
```
[Unit]
Description=Snort 3 NIDS Daemon
After=syslog.target network.target

[Service]
Type=simple
ExecStart=/usr/local/bin/snort -c /usr/local/etc/snort/snort.lua -s 65535 -k none -l /var/log/snort -D -i wlp4s0 -m 0x1b -u snort -g snort

[Install]
WantedBy=multi-user.target
```
save the file and do
`sudo systemctl daemon-reload`

#### start the service
`sudo systemctl enable --now snort3`
and check the service
`systemctl status snort3`
should be green



### use pulledpork to update your rules
`cd ~/src`
`install pulledpork`
`git clone https://github.com/shirkdog/pulledpork.git`
`cd pulledpork`

`sudo cp pulledpork.pl /usr/local/bin`
`sudo chmod +x /usr/local/bin/pulledpork.pl`
`sudo cp etc/*.conf /usr/local/etc/snort`


#### automatically run pulledpork
put this line in crontab - use the autogenerated one from the snort website
or change the time to one that your computer is likely to be on
time format is: min hour date month day - This one is 11:16am, every day
`16 11 * * * pulledpork.pl -c /usr/local/etc/snort/pulledpork.conf -i disablesid.conf -T`

edit pulledpork config
`sudo nano /usr/local/etc/snort/pulledpork.conf`
add your oinkcode
modify the paths to match those in /usr/local/etc/snort/snort.lua
mostly changing /usr/local/etc/snort/rules/
             to /usr/local/etc/rules/
`rule_path=/usr/local/etc/rules/snort.rules`
`snort_path=/usr/local/bin/snort`
`block_list=/usr/local/etc/rules/iplists/default.blocklist`
change the distro= line ????

create this folder
`sudo mkdir /usr/local/etc/rules/iplists`

test pulledpork
`sudo pulledpork.pl -c /usr/local/etc/snort/pulledpork.conf -i disablesid.conf -T`

